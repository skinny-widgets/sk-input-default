
import { SkInputImpl }  from '../../sk-input/src/impl/sk-input-impl.js';

export class DefaultSkInput extends SkInputImpl {

    get prefix() {
        return 'default';
    }

    get suffix() {
        return 'input';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }
}
